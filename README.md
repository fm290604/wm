# wm
Package wm is a terminal window manager.

Installation

    $ go get modernc.org/wm

Documentation: [godoc.org/modernc.org/wm](http://godoc.org/modernc.org/wm)

----

Demo

    $ go run demo.go

---

Screen shot (go run view_demo.go in tk/)

![](https://modernc.org/wm/blob/images/tk.png)
